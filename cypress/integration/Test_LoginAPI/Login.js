import UI from '../../Class/Functions/UI_Login_Logout'
describe('Login Test', () => {

  beforeEach(() => {
    cy.visit('/');
    
  });

  it('UI login', () =>{

    const user = Cypress.env('userName');
    const pwd = Cypress.env('password');
    const ui = new UI();
    ui.login(user,pwd);
    
 })

  it('Authentification With API custom command', () => {
    const user = Cypress.env('userName');
    const pwd = Cypress.env('password');
    const ui = new UI();
    const dashbordGraphId = 'piwik-dashboard-graphs'
    cy.LoginAPI(user, pwd);
    ui.openDashboard();
    ui.elementExiste(dashbordGraphId);
    

     
  });

})