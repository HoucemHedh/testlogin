export default class UI {

login (user, pwd) {
    cy.log('ui login');
    cy.get('#user_login').type(user);
    cy.get('#user_pass').type(pwd);
    cy.get('#loginform').submit();
}

logout () {
    cy.log('Logout');        
    cy.get('.dropdown').click();
    cy.get('a').contains('Se déconnecter').click({ force: true });
    cy.wait(2000);

}

 openDashboard(){
     cy.log('Open my dashboard');
     cy.get('li').find('a').contains('Tableau de bord').click({ force: true }); 
 }

 elementExiste (id) {
     cy.get('#'+id).should('be.visible');
     return cy.log('Element is visible');
 }


}